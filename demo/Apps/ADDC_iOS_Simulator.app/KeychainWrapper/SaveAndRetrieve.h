//
//  SaveAndRetrieve.h
//  StoringValuesInKeychain
//
//  Created by AthithChandra on 31/03/16.
//  Copyright © 2016 Wipro Technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Keychain.h"


@interface SaveAndRetrieve : NSObject

@property (nonatomic,strong)Keychain *myKeychain;

-(void) saveValuesintoKeychain:(NSString *)strKey AndValue:(NSString *)strValue;
-(void) updateItemToKeyChain:(NSString *)strKey AndValue:(NSString *)strValue;
-(NSString *) findItemInKeyChain:(NSString *)strKey;
-(void) deleteItemFromKeyChain:(NSString *)strKey;

-(BOOL) isJailbroken;
@end
