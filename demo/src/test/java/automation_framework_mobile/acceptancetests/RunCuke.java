package automation_framework_mobile.acceptancetests;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(tags = { "@loginUser", }, glue = { "automation_framework_mobile.stepdefs" }, monochrome = true, strict = true, 
plugin = { "pretty","json:target/cucumber-json/cucumber.json","junit:target/cucumber-reports/Cucumber.xml", "html:target/cucumber-reports"}, 
features = "./src/test/resources/features")

public class RunCuke {

}
