package automation_framework_mobile.utils;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class EnvConfig {
	public static String sPlatform;
	public static final int DEFAULT_TIMEOUT = 90;
	public static final int VISIBILITY_TIMEOUT = 30;
	public static final int VISIBILITY_TIMEOUT_IE = 60;
	public static String userId;
	public static String password;


	public static void getEnvironmentData() throws FileNotFoundException, IOException, ParseException {
		System.out.println("Fetching test data...");

		JSONObject jsonObject = DataMgr.getJSONDataURL("test-data.json");
		userId = (String) jsonObject.get("UserId");
		password = (String) jsonObject.get("Password");
		
		System.out.println("Fetching enviroment data...");
		
		sPlatform = System.getProperty("platform");
	
		

	}

}
