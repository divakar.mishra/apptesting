package automation_framework_mobile.utils;

import cucumber.runtime.java.guice.ScenarioScoped;

@ScenarioScoped
public class Context {
    public String sessionID;

    public String getSessionID() {
	return sessionID;
    }

    public void setSessionID(String sessionID) {
	this.sessionID = sessionID;
    }

}
