package automation_framework_mobile.stepdefs;

import org.junit.Assert;
import org.junit.Rule;
import automation_framework_mobile.pages.MyAccountPage;
import automation_framework_mobile.utils.EnvConfig;
import com.google.inject.Inject;


import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

@ScenarioScoped
public class MyAccountPageStepDef {
	@Rule
	public static AndroidDriver<MobileElement> androiddriver;
	public static IOSDriver<MobileElement> iosdriver;
	private MyAccountPage myAccountPage;

	@Inject
	public MyAccountPageStepDef(MyAccountPage myAccountPage) {
		this.myAccountPage = myAccountPage;
		if(EnvConfig.sPlatform.equalsIgnoreCase("Android"))
			androiddriver = Hooks.androiddriver;
		else if (EnvConfig.sPlatform.equalsIgnoreCase("ios"))
			iosdriver = Hooks.iosdriver;
	}
	
	@When("^User should see the My Account Page$")
	public void User_should_see_homepage() throws Throwable {
		if(EnvConfig.sPlatform.equalsIgnoreCase("Android")) {
			Assert.assertTrue("User is not on the homepage", myAccountPage.ismyAccountPageDisplayedAndroid(androiddriver));
			Thread.sleep(5000);
		}
		else if (EnvConfig.sPlatform.equalsIgnoreCase("ios"))
		{
			Assert.assertTrue("User is not on the homepage", myAccountPage.ismyAccountPageDisplayedIOS(iosdriver));
			Thread.sleep(5000);
		}
	}

}
