package automation_framework_mobile.stepdefs;

import org.junit.Assert;
import org.junit.Rule;

import automation_framework_mobile.pages.HomePage;
import automation_framework_mobile.utils.EnvConfig;
import com.google.inject.Inject;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

@ScenarioScoped
public class HomePageStepDef {

	@Rule
	public static AndroidDriver<MobileElement> androiddriver;
	public static IOSDriver<MobileElement> iosdriver;
	//private WebDriver driver;
	private HomePage homePage;

	@Inject
	public HomePageStepDef(HomePage homePage) {
		this.homePage = homePage;
		if(EnvConfig.sPlatform.equalsIgnoreCase("Android"))
			androiddriver = Hooks.androiddriver;
		else if (EnvConfig.sPlatform.equalsIgnoreCase("ios"))
			iosdriver = Hooks.iosdriver;
	}
	
	@When("^User should see the Homescreen Page$")
	public void User_should_see_homepage() throws Throwable {
		if(EnvConfig.sPlatform.equalsIgnoreCase("Android")) {
			Assert.assertTrue("User is not on the homepage", homePage.isHomePageDisplayedAndroid(androiddriver));
			Thread.sleep(5000);
		}
		else if (EnvConfig.sPlatform.equalsIgnoreCase("ios")) {
			Assert.assertTrue("User is not on the homepage", homePage.isHomePageDisplayedIOS(iosdriver));
			Thread.sleep(5000);
		}
	}
	
	@Then("^User should be able to click existingUserButton on Homescreen Page$")
	public void User_should_click_existingUserButton_homepage() throws Throwable {
		if(EnvConfig.sPlatform.equalsIgnoreCase("Android")) {
			homePage.clickExistingUserButtonAndroid(androiddriver);
			Thread.sleep(5000);
		}
		else if (EnvConfig.sPlatform.equalsIgnoreCase("ios")) {
			homePage.clickExistingUserButtonIOS(iosdriver);
			Thread.sleep(5000);
		}
	}

}
