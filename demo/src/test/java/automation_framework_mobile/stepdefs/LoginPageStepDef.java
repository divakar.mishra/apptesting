package automation_framework_mobile.stepdefs;

import org.junit.Assert;
import org.junit.Rule;
import automation_framework_mobile.pages.LoginPage;
import automation_framework_mobile.utils.EnvConfig;
import com.google.inject.Inject;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

@ScenarioScoped
public class LoginPageStepDef {
	@Rule
	public static AndroidDriver<MobileElement> androiddriver;
	public static IOSDriver<MobileElement> iosdriver;
	private LoginPage loginPage;

	@Inject
	public LoginPageStepDef(LoginPage loginPage) {
		this.loginPage = loginPage;
		if(EnvConfig.sPlatform.equalsIgnoreCase("Android"))
			androiddriver = Hooks.androiddriver;
		else if (EnvConfig.sPlatform.equalsIgnoreCase("ios"))
			iosdriver = Hooks.iosdriver;
	}
	
	
	@Then("^User should see the Login Page$")
	public void User_should_see_loginpage() throws Throwable {
		if(EnvConfig.sPlatform.equalsIgnoreCase("Android")) {
			Assert.assertTrue("User is not on the login", loginPage.isLoginPageDisplayedAndroid(androiddriver));
			Thread.sleep(2000);
		}
		else if (EnvConfig.sPlatform.equalsIgnoreCase("ios")) {
			Assert.assertTrue("User is not on the login", loginPage.isLoginPageDisplayedIOS(iosdriver));
			Thread.sleep(2000);
		}
	}
	
	@When("^User enters UserId on Login page$")
	public void User_enters_UserId() throws Throwable {
		if(EnvConfig.sPlatform.equalsIgnoreCase("Android")) {
			loginPage.enterUserIDAndroid(androiddriver);
			Thread.sleep(2000);
		}
		else if (EnvConfig.sPlatform.equalsIgnoreCase("ios")) {
			loginPage.enterUserIDIOS(iosdriver);
			Thread.sleep(2000);
		}
	}
	
	@When("^User enters Password on Login page$")
	public void User_enters_Password() throws Throwable {
		if(EnvConfig.sPlatform.equalsIgnoreCase("Android")) {
			loginPage.enterPasswordAndroid(androiddriver);
			Thread.sleep(2000);
		}
		else if (EnvConfig.sPlatform.equalsIgnoreCase("ios")) {
			loginPage.enterPasswordIOS(iosdriver);
			Thread.sleep(2000);
		}
	}
	
	@Then("^User should be able to click on the Login button on Login Page$")
	public void User_should_click_LoginButton_loginpage() throws Throwable {
		if(EnvConfig.sPlatform.equalsIgnoreCase("Android")) {
			loginPage.clickLoginButtonAndroid(androiddriver);
			Thread.sleep(30000);
		}
		else if (EnvConfig.sPlatform.equalsIgnoreCase("ios")) {
			loginPage.clickLoginButtonIOS(iosdriver);
			Thread.sleep(30000);
		}
	}

}
