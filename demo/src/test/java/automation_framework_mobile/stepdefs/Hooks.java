package automation_framework_mobile.stepdefs;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import automation_framework_mobile.utils.Selenium_Functions;
import automation_framework_mobile.utils.EnvConfig;
import automation_framework_mobile.utils.LocatorConfig;

public class Hooks {
	public static IOSDriver<MobileElement> rawiosdriver;
	public static IOSDriver<MobileElement> iosdriver;
	public static AndroidDriver<MobileElement> rawandroiddriver;
	public static AndroidDriver<MobileElement> androiddriver;

	@Before
	public void setup() throws Throwable {
		
		EnvConfig.getEnvironmentData();
		LocatorConfig.getTestData();
		
		if(EnvConfig.sPlatform.equalsIgnoreCase("Android")) {
			androiddriver = Selenium_Functions.Launch_AndroidDriver(rawandroiddriver);
			System.out.println("Android Driver Launched.....");
			
		}else if (EnvConfig.sPlatform.equalsIgnoreCase("iOS")) {
			iosdriver = Selenium_Functions.Launch_iOSDriver(rawiosdriver);
			System.out.println("iOS Driver Launched....");
		}

	}
	
	@After
	public void quit() throws InterruptedException {
		Thread.sleep(500);
		if(EnvConfig.sPlatform.equalsIgnoreCase("Android"))
			androiddriver.quit();
		else if (EnvConfig.sPlatform.equalsIgnoreCase("iOS"))
			iosdriver.quit();	
	}

}
