package automation_framework_mobile.pages;

import automation_framework_mobile.utils.EnvConfig;
import automation_framework_mobile.utils.LocatorConfig;
import automation_framework_mobile.utils.Selenium_Functions;

import cucumber.runtime.java.guice.ScenarioScoped;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

import org.junit.Assert;

@ScenarioScoped
public class LoginPage {

	
	public boolean isLoginPageDisplayedAndroid(AndroidDriver<MobileElement> driver) throws Throwable {
				Assert.assertTrue("Welcome Heading on Login Page is not Displayed", Selenium_Functions.isElementPresent(driver, LocatorConfig.heading));
				String heading = Selenium_Functions.getText(driver, LocatorConfig.heading);
				Assert.assertEquals("Homepage Heading is not correct", "Welcome to ADDC", heading);
				
				Assert.assertTrue("Welcome Heading on Login Page is not Displayed", Selenium_Functions.isElementPresent(driver, LocatorConfig.existingUserHeading));
				String existingUserHeading = Selenium_Functions.getText(driver, LocatorConfig.existingUserHeading);
				Assert.assertEquals("Existing User Heading is not correct","Existing user", existingUserHeading);
				return true;
		} 
		
	public boolean isLoginPageDisplayedIOS(IOSDriver<MobileElement> driver) throws Throwable {
				Assert.assertTrue("Welcome Heading on Login Page is not Displayed", Selenium_Functions.isElementPresent(driver, LocatorConfig.iosHeading));
				String heading = Selenium_Functions.getText(driver, LocatorConfig.iosHeading);
				Assert.assertEquals("Homepage Heading is not correct", "Welcome to ADDC", heading);
			
				Assert.assertTrue("Welcome Heading on Login Page is not Displayed", Selenium_Functions.isElementPresent(driver, LocatorConfig.iosExistingUserHeading));
				String existingUserHeading = Selenium_Functions.getText(driver, LocatorConfig.iosExistingUserHeading);
				Assert.assertEquals("Existing User Heading is not correct", "Existing user", existingUserHeading);
				return true;
		}
	
	public void enterUserIDAndroid(AndroidDriver<MobileElement> driver) throws Throwable {
		 	Selenium_Functions.enterText(driver, LocatorConfig.userIdTextBox, EnvConfig.userId);	
	}
	
	public void enterUserIDIOS(IOSDriver<MobileElement> driver) throws Throwable {
			Selenium_Functions.enterText(driver, LocatorConfig.iosUserIdTextBox, EnvConfig.userId);	
	}
	
	public void enterPasswordAndroid(AndroidDriver<MobileElement> driver) throws Throwable {
			Selenium_Functions.enterText(driver, LocatorConfig.passwordTextBox, EnvConfig.password);
	}
	
	public void enterPasswordIOS(IOSDriver<MobileElement> driver) throws Throwable {
			Selenium_Functions.enterText(driver, LocatorConfig.iosPasswordTextBox, EnvConfig.password);	
	}
	
	public void clickLoginButtonAndroid(AndroidDriver<MobileElement> driver) throws Throwable {
			Selenium_Functions.clickElement(driver, LocatorConfig.loginButton);	
	}
	
	public void clickLoginButtonIOS(IOSDriver<MobileElement> driver) throws Throwable {
			Selenium_Functions.clickElement(driver, LocatorConfig.iosLoginButton);		
	}
	
}