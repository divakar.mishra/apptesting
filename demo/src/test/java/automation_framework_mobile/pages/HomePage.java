package automation_framework_mobile.pages;


import automation_framework_mobile.utils.Selenium_Functions;
import automation_framework_mobile.utils.EnvConfig;
import automation_framework_mobile.utils.LocatorConfig;
import cucumber.runtime.java.guice.ScenarioScoped;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

import org.junit.Assert;

@ScenarioScoped
public class HomePage {
	
	public boolean isHomePageDisplayedAndroid(AndroidDriver<MobileElement> driver) throws Throwable {
			Thread.sleep(3000);
			if(Selenium_Functions.isElementPresent(driver, LocatorConfig.changeLangEnglishButton)){
				Selenium_Functions.clickElement(driver, LocatorConfig.changeLangEnglishButton);	
				Thread.sleep(5000);
				Assert.assertTrue("Welcome Heading is not Displayed", Selenium_Functions.isElementPresent(driver, LocatorConfig.heading));
				String heading = Selenium_Functions.getText(driver, LocatorConfig.heading);
				Assert.assertEquals("Homepage Heading is not correct", "Welcome to ADDC",heading);
				return true;
			}
			return false;
		
		} 
			public boolean isHomePageDisplayedIOS(IOSDriver<MobileElement> driver) throws Throwable {
			if (EnvConfig.sPlatform.equalsIgnoreCase("ios")) {
			if(Selenium_Functions.isElementPresent(driver, LocatorConfig.iosChangeLangButton)){
				Selenium_Functions.clickElement(driver, LocatorConfig.iosChangeLangButton);	
				Thread.sleep(5000);
				Assert.assertTrue("Welcome Heading is not Displayed", Selenium_Functions.isElementPresent(driver, LocatorConfig.iosHeading));
				String heading = Selenium_Functions.getText(driver, LocatorConfig.iosHeading);
				Assert.assertEquals("Homepage Heading is not correct", "Welcome to ADDC",heading);
				return true;
			}
		}
			return false;
	}
	
	public void clickExistingUserButtonAndroid(AndroidDriver<MobileElement>  driver) throws Throwable {
			Selenium_Functions.clickElement(driver, LocatorConfig.existingUserButton);
	}
	public void clickExistingUserButtonIOS(IOSDriver<MobileElement>  driver) throws Throwable {
			Selenium_Functions.clickElement(driver, LocatorConfig.iosExistingUserButton);	
			
	}
	
}