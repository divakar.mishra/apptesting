package automation_framework_mobile.juice;

import automation_framework_mobile.utils.Context;
import com.google.inject.AbstractModule;

import cucumber.api.guice.CucumberScopes;

public final class GuiceModule extends AbstractModule {
    @SuppressWarnings("deprecation")
	@Override
    public void configure() {
	try {
	    // Bindings for classes that are shared for the lifetime of the
	    // execution.
	    // bind(Context.class).in(Scopes.SINGLETON);
	    // Bindings for classes that are shared for the lifetime of the
	    // scenario.
	    bind(Context.class).in(CucumberScopes.SCENARIO);
	} catch (Exception e) {
	    addError(e.getMessage());
	}
    }
}
