Feature: Login User Valid
Login for existing user

@loginUser
Scenario: ADDC User Login
    When User should see the Homescreen Page
    Then User should be able to click existingUserButton on Homescreen Page
    And User should see the Login Page
    Then User enters UserId on Login page
    And User enters Password on Login page
    When User should be able to click on the Login button on Login Page
    Then User should see the My Account Page